//
//  TcpConnection.m
//  TcpExample
//
//  Created by Nordin on 23-10-13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "TcpConnection.h"


@implementation TcpConnection

- (void)initNetworkCommunication {
	NSLog(@"initNetworkCommunication.\n");
	
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    
	CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)@"127.0.0.1", 2222, &readStream, &writeStream);
    
	inputStream = (NSInputStream *)readStream;
    outputStream = (NSOutputStream *)writeStream;
	
	fhStdIn = [[NSFileHandle fileHandleWithStandardInput] retain];
	
	
	// Each thread has its own NSRunLoop object by default. So as you see
	// we're using the main-thread's NSRunLoop's object. See Apple's doc for more info.
	myRunLoop = [NSRunLoop currentRunLoop];
	[inputStream scheduleInRunLoop: myRunLoop forMode:NSDefaultRunLoopMode];	

}

- (void) initDelegate {
	NSLog(@"initDelegate.\n");
	[inputStream setDelegate:self];
}

- (void) sendEchoMessage: (NSString*) message {
	//NSLog(@"sendEchoMessage.\n");
	NSData *data = [[NSData alloc] initWithData:[message dataUsingEncoding:NSASCIIStringEncoding]];
	[outputStream write:[data bytes] maxLength:[data length]];
	//NSLog(@"sendEchoMessage - message has sent!\n");
}

- (void) fileHandlerCompletion: (NSNotification*) notification {
	//NSLog(@"fileHandlerCompletion.\n");
	NSData *data = [[notification userInfo]
					objectForKey:NSFileHandleNotificationDataItem];
	NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
	NSString *sender = [NSString stringWithFormat:@"<client> %@", string];
	[self sendEchoMessage:sender];
	[string release];
	[fhStdIn readInBackgroundAndNotify]; // this must be called again
}
- (void) openConnection {
	NSLog(@"openConnection.\n");
	[inputStream open];
	[outputStream open];
	
	ntfyCentr = [NSNotificationCenter defaultCenter];
	[ntfyCentr addObserver: self
				  selector: @selector(fileHandlerCompletion:)
					  name: NSFileHandleReadCompletionNotification
					object:fhStdIn];
	[fhStdIn readInBackgroundAndNotify];
}


// Dit is the callback van NSStreamDelegate
- (void) stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode {
	//NSLog(@"stream event %i", eventCode);
	
	switch (eventCode) {
		case NSStreamEventOpenCompleted:
			NSLog(@"Stream opened!\n");
			//[self sendEchoMessage]; // Hier blokt schrijven
			break;			
		case NSStreamEventHasBytesAvailable:
			//NSLog(@"Stream has bytes available!\n");
			if (aStream == inputStream) {
				
				uint8_t buffer[1024];
				int len;
				
				while ([inputStream hasBytesAvailable]) {
					len = [inputStream read:buffer maxLength:sizeof(buffer)];
					if (len > 0) {
						
						NSString *output = [[NSString alloc] initWithBytes:buffer length:len encoding:NSASCIIStringEncoding];
						
						if (nil != output) {
							NSLog(@"<server> %@", output);
						}
					}
				}
			}
			//[self sendEchoMessage: @"Werkt dit?\n"];
			break;			
		case NSStreamEventErrorOccurred:
			NSLog(@"Attention! Can not connect to the host!\n");
			break;
		case NSStreamEventEndEncountered:
			NSLog(@"End of stream encountered.\n");
			break;
		default:
			NSLog(@"Unknown event...\n");
	}
}

- (void) runConnection {
	NSLog(@"starting runConnection.\n");
	[myRunLoop run]; 
	NSLog(@"leaving runConnection.\n");
}

@end
