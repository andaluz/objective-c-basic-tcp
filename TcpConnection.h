//
//  TcpConnection.h
//  TcpExample
//
//  Created by Nordin on 23-10-13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//




@interface TcpConnection : NSObject <NSStreamDelegate> {
	NSInputStream *inputStream;
	NSOutputStream *outputStream;
	NSRunLoop *myRunLoop;
	NSFileHandle *fhStdIn;
	NSNotificationCenter *ntfyCentr;
}

- (void)initNetworkCommunication;

- (void) initDelegate;

- (void) openConnection;

- (void) runConnection;

@end
