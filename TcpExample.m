#import <Foundation/Foundation.h>
#import "TcpConnection.h"

int main (int argc, const char * argv[]) {
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	
    NSLog(@"Starting connection...\n");
	TcpConnection *conn = [[TcpConnection alloc] init];
	[conn initNetworkCommunication];
	[conn initDelegate];
	[conn openConnection];
	[conn runConnection];
	
    [pool drain];
    return 0;
}
